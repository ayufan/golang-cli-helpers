package clihelpers

import (
	"strings"
	"testing"
)

type Parent struct {
	String    string `short:"s" long:"string" description:"desc" env:"STRING"`
	Bytes     []byte `long:"bytes" env:"@BYTES"`
	Struct    Child  `namespace:"namespaced"`
	StructPtr *Child `namespace:"ptr"`
}

type Child struct {
	Float   float64 `long:"float" env:"FLOAT"`
	Integer int     `long:"integer" env:"@INTEGER"`
	BoolPtr *bool   `long:"boolptr" env:"BOOL_PTR"`

	Nested Grandchild `namespace:"grandchild"`
}

type Grandchild struct {
	Bool bool `long:"bool" env:"@BOOL"`
}

func TestFlagsFromStruct(t *testing.T) {
	var config Parent
	config.StructPtr = &Child{}

	flags := GetFlagsFromStruct(config)

	expected := []string{
		`-s value, --string value	desc [$STRING]`,
		`--bytes value	 [$BYTES]`,
		`--namespaced-float value	(default: "0") [$FLOAT]`,
		`--namespaced-integer value	(default: "0") [$NAMESPACED_INTEGER]`,
		`--namespaced-boolptr value	 [$BOOL_PTR]`,
		`--namespaced-grandchild-bool	 [$NAMESPACED_GRANDCHILD_BOOL]`,
		`--ptr-float value	(default: "0") [$FLOAT]`,
		`--ptr-integer value	(default: "0") [$PTR_INTEGER]`,
		`--ptr-boolptr value	 [$BOOL_PTR]`,
		`--ptr-grandchild-bool	 [$PTR_GRANDCHILD_BOOL]`,
	}

	var v []string
	for _, flag := range flags {
		v = append(v, flag.String())
	}

	if a, b := strings.Join(v, "\n"), strings.Join(expected, "\n"); a != b {
		t.Fatalf("\nexpected: \n%v\n\ngot: \n%v", b, a)
	}
}
